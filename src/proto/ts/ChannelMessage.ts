export interface UserChannelChatInfoDTO {
  headPic?: string;
  userid?: Long;
  nickname?: string;
  level?: number;
  rankName?: string;
}

export function encodeUserChannelChatInfoDTO(message: UserChannelChatInfoDTO): Uint8Array {
  const bb = popByteBuffer();
  _encodeUserChannelChatInfoDTO(message, bb);
  return toUint8Array(bb);
}

function _encodeUserChannelChatInfoDTO(message: UserChannelChatInfoDTO, bb: ByteBuffer): void {
  // optional string headPic = 1;
  const $headPic = message.headPic;
  if ($headPic !== undefined) {
    writeVarint32(bb, 10);
    writeString(bb, $headPic);
  }

  // optional int64 userid = 2;
  const $userid = message.userid;
  if ($userid !== undefined) {
    writeVarint32(bb, 16);
    writeVarint64(bb, $userid);
  }

  // optional string nickname = 3;
  const $nickname = message.nickname;
  if ($nickname !== undefined) {
    writeVarint32(bb, 26);
    writeString(bb, $nickname);
  }

  // optional int32 level = 4;
  const $level = message.level;
  if ($level !== undefined) {
    writeVarint32(bb, 32);
    writeVarint64(bb, intToLong($level));
  }

  // optional string rankName = 5;
  const $rankName = message.rankName;
  if ($rankName !== undefined) {
    writeVarint32(bb, 42);
    writeString(bb, $rankName);
  }
}

export function decodeUserChannelChatInfoDTO(binary: Uint8Array): UserChannelChatInfoDTO {
  return _decodeUserChannelChatInfoDTO(wrapByteBuffer(binary));
}

function _decodeUserChannelChatInfoDTO(bb: ByteBuffer): UserChannelChatInfoDTO {
  const message: UserChannelChatInfoDTO = {} as any;

  end_of_message: while (!isAtEnd(bb)) {
    const tag = readVarint32(bb);

    switch (tag >>> 3) {
      case 0:
        break end_of_message;

      // optional string headPic = 1;
      case 1: {
        message.headPic = readString(bb, readVarint32(bb));
        break;
      }

      // optional int64 userid = 2;
      case 2: {
        message.userid = readVarint64(bb, /* unsigned */ false);
        break;
      }

      // optional string nickname = 3;
      case 3: {
        message.nickname = readString(bb, readVarint32(bb));
        break;
      }

      // optional int32 level = 4;
      case 4: {
        message.level = readVarint32(bb);
        break;
      }

      // optional string rankName = 5;
      case 5: {
        message.rankName = readString(bb, readVarint32(bb));
        break;
      }

      default:
        skipUnknownField(bb, tag & 7);
    }
  }

  return message;
}

export interface ChannelMessageDTO {
  content?: string;
  channelId?: Long;
  type?: number;
  userid?: Long;
  headPic?: string;
}

export function encodeChannelMessageDTO(message: ChannelMessageDTO): Uint8Array {
  const bb = popByteBuffer();
  _encodeChannelMessageDTO(message, bb);
  return toUint8Array(bb);
}

function _encodeChannelMessageDTO(message: ChannelMessageDTO, bb: ByteBuffer): void {
  // optional string content = 1;
  const $content = message.content;
  if ($content !== undefined) {
    writeVarint32(bb, 10);
    writeString(bb, $content);
  }

  // optional int64 channelId = 2;
  const $channelId = message.channelId;
  if ($channelId !== undefined) {
    writeVarint32(bb, 16);
    writeVarint64(bb, $channelId);
  }

  // optional int32 type = 3;
  const $type = message.type;
  if ($type !== undefined) {
    writeVarint32(bb, 24);
    writeVarint64(bb, intToLong($type));
  }

  // optional int64 userid = 4;
  const $userid = message.userid;
  if ($userid !== undefined) {
    writeVarint32(bb, 32);
    writeVarint64(bb, $userid);
  }

  // optional string headPic = 5;
  const $headPic = message.headPic;
  if ($headPic !== undefined) {
    writeVarint32(bb, 42);
    writeString(bb, $headPic);
  }
}

export function decodeChannelMessageDTO(binary: Uint8Array): ChannelMessageDTO {
  return _decodeChannelMessageDTO(wrapByteBuffer(binary));
}

function _decodeChannelMessageDTO(bb: ByteBuffer): ChannelMessageDTO {
  const message: ChannelMessageDTO = {} as any;

  end_of_message: while (!isAtEnd(bb)) {
    const tag = readVarint32(bb);

    switch (tag >>> 3) {
      case 0:
        break end_of_message;

      // optional string content = 1;
      case 1: {
        message.content = readString(bb, readVarint32(bb));
        break;
      }

      // optional int64 channelId = 2;
      case 2: {
        message.channelId = readVarint64(bb, /* unsigned */ false);
        break;
      }

      // optional int32 type = 3;
      case 3: {
        message.type = readVarint32(bb);
        break;
      }

      // optional int64 userid = 4;
      case 4: {
        message.userid = readVarint64(bb, /* unsigned */ false);
        break;
      }

      // optional string headPic = 5;
      case 5: {
        message.headPic = readString(bb, readVarint32(bb));
        break;
      }

      default:
        skipUnknownField(bb, tag & 7);
    }
  }

  return message;
}

export interface UserSendMessage_1004 {
  message?: ChannelMessageDTO;
}

export function encodeUserSendMessage_1004(message: UserSendMessage_1004): Uint8Array {
  const bb = popByteBuffer();
  _encodeUserSendMessage_1004(message, bb);
  return toUint8Array(bb);
}

function _encodeUserSendMessage_1004(message: UserSendMessage_1004, bb: ByteBuffer): void {
  // optional ChannelMessageDTO message = 1;
  const $message = message.message;
  if ($message !== undefined) {
    writeVarint32(bb, 10);
    const nested = popByteBuffer();
    _encodeChannelMessageDTO($message, nested);
    writeVarint32(bb, nested.limit);
    writeByteBuffer(bb, nested);
    pushByteBuffer(nested);
  }
}

export function decodeUserSendMessage_1004(binary: Uint8Array): UserSendMessage_1004 {
  return _decodeUserSendMessage_1004(wrapByteBuffer(binary));
}

function _decodeUserSendMessage_1004(bb: ByteBuffer): UserSendMessage_1004 {
  const message: UserSendMessage_1004 = {} as any;

  end_of_message: while (!isAtEnd(bb)) {
    const tag = readVarint32(bb);

    switch (tag >>> 3) {
      case 0:
        break end_of_message;

      // optional ChannelMessageDTO message = 1;
      case 1: {
        const limit = pushTemporaryLength(bb);
        message.message = _decodeChannelMessageDTO(bb);
        bb.limit = limit;
        break;
      }

      default:
        skipUnknownField(bb, tag & 7);
    }
  }

  return message;
}

export interface UserReceiveMessage_1005 {
  message?: ChannelMessageDTO;
}

export function encodeUserReceiveMessage_1005(message: UserReceiveMessage_1005): Uint8Array {
  const bb = popByteBuffer();
  _encodeUserReceiveMessage_1005(message, bb);
  return toUint8Array(bb);
}

function _encodeUserReceiveMessage_1005(message: UserReceiveMessage_1005, bb: ByteBuffer): void {
  // optional ChannelMessageDTO message = 1;
  const $message = message.message;
  if ($message !== undefined) {
    writeVarint32(bb, 10);
    const nested = popByteBuffer();
    _encodeChannelMessageDTO($message, nested);
    writeVarint32(bb, nested.limit);
    writeByteBuffer(bb, nested);
    pushByteBuffer(nested);
  }
}

export function decodeUserReceiveMessage_1005(binary: Uint8Array): UserReceiveMessage_1005 {
  return _decodeUserReceiveMessage_1005(wrapByteBuffer(binary));
}

function _decodeUserReceiveMessage_1005(bb: ByteBuffer): UserReceiveMessage_1005 {
  const message: UserReceiveMessage_1005 = {} as any;

  end_of_message: while (!isAtEnd(bb)) {
    const tag = readVarint32(bb);

    switch (tag >>> 3) {
      case 0:
        break end_of_message;

      // optional ChannelMessageDTO message = 1;
      case 1: {
        const limit = pushTemporaryLength(bb);
        message.message = _decodeChannelMessageDTO(bb);
        bb.limit = limit;
        break;
      }

      default:
        skipUnknownField(bb, tag & 7);
    }
  }

  return message;
}

export interface UserJoinChannelNotice_1006 {
  channelId?: Long;
  userInfo?: UserChannelChatInfoDTO;
}

export function encodeUserJoinChannelNotice_1006(message: UserJoinChannelNotice_1006): Uint8Array {
  const bb = popByteBuffer();
  _encodeUserJoinChannelNotice_1006(message, bb);
  return toUint8Array(bb);
}

function _encodeUserJoinChannelNotice_1006(message: UserJoinChannelNotice_1006, bb: ByteBuffer): void {
  // optional int64 channelId = 1;
  const $channelId = message.channelId;
  if ($channelId !== undefined) {
    writeVarint32(bb, 8);
    writeVarint64(bb, $channelId);
  }

  // optional UserChannelChatInfoDTO userInfo = 2;
  const $userInfo = message.userInfo;
  if ($userInfo !== undefined) {
    writeVarint32(bb, 18);
    const nested = popByteBuffer();
    _encodeUserChannelChatInfoDTO($userInfo, nested);
    writeVarint32(bb, nested.limit);
    writeByteBuffer(bb, nested);
    pushByteBuffer(nested);
  }
}

export function decodeUserJoinChannelNotice_1006(binary: Uint8Array): UserJoinChannelNotice_1006 {
  return _decodeUserJoinChannelNotice_1006(wrapByteBuffer(binary));
}

function _decodeUserJoinChannelNotice_1006(bb: ByteBuffer): UserJoinChannelNotice_1006 {
  const message: UserJoinChannelNotice_1006 = {} as any;

  end_of_message: while (!isAtEnd(bb)) {
    const tag = readVarint32(bb);

    switch (tag >>> 3) {
      case 0:
        break end_of_message;

      // optional int64 channelId = 1;
      case 1: {
        message.channelId = readVarint64(bb, /* unsigned */ false);
        break;
      }

      // optional UserChannelChatInfoDTO userInfo = 2;
      case 2: {
        const limit = pushTemporaryLength(bb);
        message.userInfo = _decodeUserChannelChatInfoDTO(bb);
        bb.limit = limit;
        break;
      }

      default:
        skipUnknownField(bb, tag & 7);
    }
  }

  return message;
}

export interface UserOnlineStatusNotice_1007 {
  channelId?: Long;
  userid?: Long;
  online?: boolean;
}

export function encodeUserOnlineStatusNotice_1007(message: UserOnlineStatusNotice_1007): Uint8Array {
  const bb = popByteBuffer();
  _encodeUserOnlineStatusNotice_1007(message, bb);
  return toUint8Array(bb);
}

function _encodeUserOnlineStatusNotice_1007(message: UserOnlineStatusNotice_1007, bb: ByteBuffer): void {
  // optional int64 channelId = 1;
  const $channelId = message.channelId;
  if ($channelId !== undefined) {
    writeVarint32(bb, 8);
    writeVarint64(bb, $channelId);
  }

  // optional int64 userid = 2;
  const $userid = message.userid;
  if ($userid !== undefined) {
    writeVarint32(bb, 16);
    writeVarint64(bb, $userid);
  }

  // optional bool online = 3;
  const $online = message.online;
  if ($online !== undefined) {
    writeVarint32(bb, 24);
    writeByte(bb, $online ? 1 : 0);
  }
}

export function decodeUserOnlineStatusNotice_1007(binary: Uint8Array): UserOnlineStatusNotice_1007 {
  return _decodeUserOnlineStatusNotice_1007(wrapByteBuffer(binary));
}

function _decodeUserOnlineStatusNotice_1007(bb: ByteBuffer): UserOnlineStatusNotice_1007 {
  const message: UserOnlineStatusNotice_1007 = {} as any;

  end_of_message: while (!isAtEnd(bb)) {
    const tag = readVarint32(bb);

    switch (tag >>> 3) {
      case 0:
        break end_of_message;

      // optional int64 channelId = 1;
      case 1: {
        message.channelId = readVarint64(bb, /* unsigned */ false);
        break;
      }

      // optional int64 userid = 2;
      case 2: {
        message.userid = readVarint64(bb, /* unsigned */ false);
        break;
      }

      // optional bool online = 3;
      case 3: {
        message.online = !!readByte(bb);
        break;
      }

      default:
        skipUnknownField(bb, tag & 7);
    }
  }

  return message;
}

export interface Long {
  low: number;
  high: number;
  unsigned: boolean;
}

interface ByteBuffer {
  bytes: Uint8Array;
  offset: number;
  limit: number;
}

function pushTemporaryLength(bb: ByteBuffer): number {
  const length = readVarint32(bb);
  const { limit } = bb;
  bb.limit = bb.offset + length;
  return limit;
}

function skipUnknownField(bb: ByteBuffer, type: number): void {
  switch (type) {
    case 0: while (readByte(bb) & 0x80) { } break;
    case 2: skip(bb, readVarint32(bb)); break;
    case 5: skip(bb, 4); break;
    case 1: skip(bb, 8); break;
    default: throw new Error(`Unimplemented type: ${type}`);
  }
}

function stringToLong(value: string): Long {
  return {
    low: value.charCodeAt(0) | (value.charCodeAt(1) << 16),
    high: value.charCodeAt(2) | (value.charCodeAt(3) << 16),
    unsigned: false,
  };
}

function longToString(value: Long): string {
  const { low } = value;
  const { high } = value;
  return String.fromCharCode(
    low & 0xFFFF,
    low >>> 16,
    high & 0xFFFF,
    high >>> 16,
  );
}

// The code below was modified from https://github.com/protobufjs/bytebuffer.js
// which is under the Apache License 2.0.

const f32 = new Float32Array(1);
const f32_u8 = new Uint8Array(f32.buffer);

const f64 = new Float64Array(1);
const f64_u8 = new Uint8Array(f64.buffer);

function intToLong(value: number): Long {
  value |= 0;
  return {
    low: value,
    high: value >> 31,
    unsigned: value >= 0,
  };
}

const bbStack: ByteBuffer[] = [];

function popByteBuffer(): ByteBuffer {
  const bb = bbStack.pop();
  if (!bb) return { bytes: new Uint8Array(64), offset: 0, limit: 0 };
  bb.offset = bb.limit = 0;
  return bb;
}

function pushByteBuffer(bb: ByteBuffer): void {
  bbStack.push(bb);
}

function wrapByteBuffer(bytes: Uint8Array): ByteBuffer {
  return { bytes, offset: 0, limit: bytes.length };
}

function toUint8Array(bb: ByteBuffer): Uint8Array {
  const { bytes } = bb;
  const { limit } = bb;
  return bytes.length === limit ? bytes : bytes.subarray(0, limit);
}

function skip(bb: ByteBuffer, offset: number): void {
  if (bb.offset + offset > bb.limit) {
    throw new Error('Skip past limit');
  }
  bb.offset += offset;
}

function isAtEnd(bb: ByteBuffer): boolean {
  return bb.offset >= bb.limit;
}

function grow(bb: ByteBuffer, count: number): number {
  const { bytes } = bb;
  const { offset } = bb;
  const { limit } = bb;
  const finalOffset = offset + count;
  if (finalOffset > bytes.length) {
    const newBytes = new Uint8Array(finalOffset * 2);
    newBytes.set(bytes);
    bb.bytes = newBytes;
  }
  bb.offset = finalOffset;
  if (finalOffset > limit) {
    bb.limit = finalOffset;
  }
  return offset;
}

function advance(bb: ByteBuffer, count: number): number {
  const { offset } = bb;
  if (offset + count > bb.limit) {
    throw new Error('Read past limit');
  }
  bb.offset += count;
  return offset;
}

function readBytes(bb: ByteBuffer, count: number): Uint8Array {
  const offset = advance(bb, count);
  return bb.bytes.subarray(offset, offset + count);
}

function writeBytes(bb: ByteBuffer, buffer: Uint8Array): void {
  const offset = grow(bb, buffer.length);
  bb.bytes.set(buffer, offset);
}

function readString(bb: ByteBuffer, count: number): string {
  // Sadly a hand-coded UTF8 decoder is much faster than subarray+TextDecoder in V8
  const offset = advance(bb, count);
  const { fromCharCode } = String;
  const { bytes } = bb;
  const invalid = '\uFFFD';
  let text = '';

  for (let i = 0; i < count; i++) {
    const c1 = bytes[i + offset]; let c2: number; let c3: number; let c4: number; let
      c: number;

    // 1 byte
    if ((c1 & 0x80) === 0) {
      text += fromCharCode(c1);
    }

    // 2 bytes
    else if ((c1 & 0xE0) === 0xC0) {
      if (i + 1 >= count) text += invalid;
      else {
        c2 = bytes[i + offset + 1];
        if ((c2 & 0xC0) !== 0x80) text += invalid;
        else {
          c = ((c1 & 0x1F) << 6) | (c2 & 0x3F);
          if (c < 0x80) text += invalid;
          else {
            text += fromCharCode(c);
            i++;
          }
        }
      }
    }

    // 3 bytes
    else if ((c1 & 0xF0) == 0xE0) {
      if (i + 2 >= count) text += invalid;
      else {
        c2 = bytes[i + offset + 1];
        c3 = bytes[i + offset + 2];
        if (((c2 | (c3 << 8)) & 0xC0C0) !== 0x8080) text += invalid;
        else {
          c = ((c1 & 0x0F) << 12) | ((c2 & 0x3F) << 6) | (c3 & 0x3F);
          if (c < 0x0800 || (c >= 0xD800 && c <= 0xDFFF)) text += invalid;
          else {
            text += fromCharCode(c);
            i += 2;
          }
        }
      }
    }

    // 4 bytes
    else if ((c1 & 0xF8) == 0xF0) {
      if (i + 3 >= count) text += invalid;
      else {
        c2 = bytes[i + offset + 1];
        c3 = bytes[i + offset + 2];
        c4 = bytes[i + offset + 3];
        if (((c2 | (c3 << 8) | (c4 << 16)) & 0xC0C0C0) !== 0x808080) text += invalid;
        else {
          c = ((c1 & 0x07) << 0x12) | ((c2 & 0x3F) << 0x0C) | ((c3 & 0x3F) << 0x06) | (c4 & 0x3F);
          if (c < 0x10000 || c > 0x10FFFF) text += invalid;
          else {
            c -= 0x10000;
            text += fromCharCode((c >> 10) + 0xD800, (c & 0x3FF) + 0xDC00);
            i += 3;
          }
        }
      }
    } else text += invalid;
  }

  return text;
}

function writeString(bb: ByteBuffer, text: string): void {
  // Sadly a hand-coded UTF8 encoder is much faster than TextEncoder+set in V8
  const n = text.length;
  let byteCount = 0;

  // Write the byte count first
  for (let i = 0; i < n; i++) {
    let c = text.charCodeAt(i);
    if (c >= 0xD800 && c <= 0xDBFF && i + 1 < n) {
      c = (c << 10) + text.charCodeAt(++i) - 0x35FDC00;
    }
    byteCount += c < 0x80 ? 1 : c < 0x800 ? 2 : c < 0x10000 ? 3 : 4;
  }
  writeVarint32(bb, byteCount);

  let offset = grow(bb, byteCount);
  const { bytes } = bb;

  // Then write the bytes
  for (let i = 0; i < n; i++) {
    let c = text.charCodeAt(i);
    if (c >= 0xD800 && c <= 0xDBFF && i + 1 < n) {
      c = (c << 10) + text.charCodeAt(++i) - 0x35FDC00;
    }
    if (c < 0x80) {
      bytes[offset++] = c;
    } else {
      if (c < 0x800) {
        bytes[offset++] = ((c >> 6) & 0x1F) | 0xC0;
      } else {
        if (c < 0x10000) {
          bytes[offset++] = ((c >> 12) & 0x0F) | 0xE0;
        } else {
          bytes[offset++] = ((c >> 18) & 0x07) | 0xF0;
          bytes[offset++] = ((c >> 12) & 0x3F) | 0x80;
        }
        bytes[offset++] = ((c >> 6) & 0x3F) | 0x80;
      }
      bytes[offset++] = (c & 0x3F) | 0x80;
    }
  }
}

function writeByteBuffer(bb: ByteBuffer, buffer: ByteBuffer): void {
  const offset = grow(bb, buffer.limit);
  const from = bb.bytes;
  const to = buffer.bytes;

  // This for loop is much faster than subarray+set on V8
  for (let i = 0, n = buffer.limit; i < n; i++) {
    from[i + offset] = to[i];
  }
}

function readByte(bb: ByteBuffer): number {
  return bb.bytes[advance(bb, 1)];
}

function writeByte(bb: ByteBuffer, value: number): void {
  const offset = grow(bb, 1);
  bb.bytes[offset] = value;
}

function readFloat(bb: ByteBuffer): number {
  let offset = advance(bb, 4);
  const { bytes } = bb;

  // Manual copying is much faster than subarray+set in V8
  f32_u8[0] = bytes[offset++];
  f32_u8[1] = bytes[offset++];
  f32_u8[2] = bytes[offset++];
  f32_u8[3] = bytes[offset++];
  return f32[0];
}

function writeFloat(bb: ByteBuffer, value: number): void {
  let offset = grow(bb, 4);
  const { bytes } = bb;
  f32[0] = value;

  // Manual copying is much faster than subarray+set in V8
  bytes[offset++] = f32_u8[0];
  bytes[offset++] = f32_u8[1];
  bytes[offset++] = f32_u8[2];
  bytes[offset++] = f32_u8[3];
}

function readDouble(bb: ByteBuffer): number {
  let offset = advance(bb, 8);
  const { bytes } = bb;

  // Manual copying is much faster than subarray+set in V8
  f64_u8[0] = bytes[offset++];
  f64_u8[1] = bytes[offset++];
  f64_u8[2] = bytes[offset++];
  f64_u8[3] = bytes[offset++];
  f64_u8[4] = bytes[offset++];
  f64_u8[5] = bytes[offset++];
  f64_u8[6] = bytes[offset++];
  f64_u8[7] = bytes[offset++];
  return f64[0];
}

function writeDouble(bb: ByteBuffer, value: number): void {
  let offset = grow(bb, 8);
  const { bytes } = bb;
  f64[0] = value;

  // Manual copying is much faster than subarray+set in V8
  bytes[offset++] = f64_u8[0];
  bytes[offset++] = f64_u8[1];
  bytes[offset++] = f64_u8[2];
  bytes[offset++] = f64_u8[3];
  bytes[offset++] = f64_u8[4];
  bytes[offset++] = f64_u8[5];
  bytes[offset++] = f64_u8[6];
  bytes[offset++] = f64_u8[7];
}

function readInt32(bb: ByteBuffer): number {
  const offset = advance(bb, 4);
  const { bytes } = bb;
  return (
    bytes[offset]
    | (bytes[offset + 1] << 8)
    | (bytes[offset + 2] << 16)
    | (bytes[offset + 3] << 24)
  );
}

function writeInt32(bb: ByteBuffer, value: number): void {
  const offset = grow(bb, 4);
  const { bytes } = bb;
  bytes[offset] = value;
  bytes[offset + 1] = value >> 8;
  bytes[offset + 2] = value >> 16;
  bytes[offset + 3] = value >> 24;
}

function readInt64(bb: ByteBuffer, unsigned: boolean): Long {
  return {
    low: readInt32(bb),
    high: readInt32(bb),
    unsigned,
  };
}

function writeInt64(bb: ByteBuffer, value: Long): void {
  writeInt32(bb, value.low);
  writeInt32(bb, value.high);
}

function readVarint32(bb: ByteBuffer): number {
  let c = 0;
  let value = 0;
  let b: number;
  do {
    b = readByte(bb);
    if (c < 32) value |= (b & 0x7F) << c;
    c += 7;
  } while (b & 0x80);
  return value;
}

function writeVarint32(bb: ByteBuffer, value: number): void {
  value >>>= 0;
  while (value >= 0x80) {
    writeByte(bb, (value & 0x7f) | 0x80);
    value >>>= 7;
  }
  writeByte(bb, value);
}

function readVarint64(bb: ByteBuffer, unsigned: boolean): Long {
  let part0 = 0;
  let part1 = 0;
  let part2 = 0;
  let b: number;

  b = readByte(bb); part0 = (b & 0x7F); if (b & 0x80) {
    b = readByte(bb); part0 |= (b & 0x7F) << 7; if (b & 0x80) {
      b = readByte(bb); part0 |= (b & 0x7F) << 14; if (b & 0x80) {
        b = readByte(bb); part0 |= (b & 0x7F) << 21; if (b & 0x80) {
          b = readByte(bb); part1 = (b & 0x7F); if (b & 0x80) {
            b = readByte(bb); part1 |= (b & 0x7F) << 7; if (b & 0x80) {
              b = readByte(bb); part1 |= (b & 0x7F) << 14; if (b & 0x80) {
                b = readByte(bb); part1 |= (b & 0x7F) << 21; if (b & 0x80) {
                  b = readByte(bb); part2 = (b & 0x7F); if (b & 0x80) {
                    b = readByte(bb); part2 |= (b & 0x7F) << 7;
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  return {
    low: part0 | (part1 << 28),
    high: (part1 >>> 4) | (part2 << 24),
    unsigned,
  };
}

function writeVarint64(bb: ByteBuffer, value: Long): void {
  const part0 = value.low >>> 0;
  const part1 = ((value.low >>> 28) | (value.high << 4)) >>> 0;
  const part2 = value.high >>> 24;

  // ref: src/google/protobuf/io/coded_stream.cc
  const size = part2 === 0
    ? part1 === 0
      ? part0 < 1 << 14
        ? part0 < 1 << 7 ? 1 : 2
        : part0 < 1 << 21 ? 3 : 4
      : part1 < 1 << 14
        ? part1 < 1 << 7 ? 5 : 6
        : part1 < 1 << 21 ? 7 : 8
    : part2 < 1 << 7 ? 9 : 10;

  const offset = grow(bb, size);
  const { bytes } = bb;

  switch (size) {
    case 10: bytes[offset + 9] = (part2 >>> 7) & 0x01;
    case 9: bytes[offset + 8] = size !== 9 ? part2 | 0x80 : part2 & 0x7F;
    case 8: bytes[offset + 7] = size !== 8 ? (part1 >>> 21) | 0x80 : (part1 >>> 21) & 0x7F;
    case 7: bytes[offset + 6] = size !== 7 ? (part1 >>> 14) | 0x80 : (part1 >>> 14) & 0x7F;
    case 6: bytes[offset + 5] = size !== 6 ? (part1 >>> 7) | 0x80 : (part1 >>> 7) & 0x7F;
    case 5: bytes[offset + 4] = size !== 5 ? part1 | 0x80 : part1 & 0x7F;
    case 4: bytes[offset + 3] = size !== 4 ? (part0 >>> 21) | 0x80 : (part0 >>> 21) & 0x7F;
    case 3: bytes[offset + 2] = size !== 3 ? (part0 >>> 14) | 0x80 : (part0 >>> 14) & 0x7F;
    case 2: bytes[offset + 1] = size !== 2 ? (part0 >>> 7) | 0x80 : (part0 >>> 7) & 0x7F;
    case 1: bytes[offset] = size !== 1 ? part0 | 0x80 : part0 & 0x7F;
  }
}

function readVarint32ZigZag(bb: ByteBuffer): number {
  const value = readVarint32(bb);

  // ref: src/google/protobuf/wire_format_lite.h
  return (value >>> 1) ^ -(value & 1);
}

function writeVarint32ZigZag(bb: ByteBuffer, value: number): void {
  // ref: src/google/protobuf/wire_format_lite.h
  writeVarint32(bb, (value << 1) ^ (value >> 31));
}

function readVarint64ZigZag(bb: ByteBuffer): Long {
  const value = readVarint64(bb, /* unsigned */ false);
  const { low } = value;
  const { high } = value;
  const flip = -(low & 1);

  // ref: src/google/protobuf/wire_format_lite.h
  return {
    low: ((low >>> 1) | (high << 31)) ^ flip,
    high: (high >>> 1) ^ flip,
    unsigned: false,
  };
}

function writeVarint64ZigZag(bb: ByteBuffer, value: Long): void {
  const { low } = value;
  const { high } = value;
  const flip = high >> 31;

  // ref: src/google/protobuf/wire_format_lite.h
  writeVarint64(bb, {
    low: (low << 1) ^ flip,
    high: ((high << 1) | (low >>> 31)) ^ flip,
    unsigned: false,
  });
}
