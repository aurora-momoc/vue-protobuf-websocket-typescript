import {
  encodesessionBuildResponse_100, decodesessionBuildResponse_100,
} from '@/proto/ts/SystemMessage';
import IMessage from '../../IMessage';
export class SessionBuildResponse_100 implements IMessage {

  static classname = "SessionBuildResponse_100";

  constructor() {
  }

  decode(obj: any) {
    return decodesessionBuildResponse_100(obj);
  }

  encode(obj: any) {
    return encodesessionBuildResponse_100(obj);
  }
}
