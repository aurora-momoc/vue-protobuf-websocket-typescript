import {
  decodeheartPopMessage_200, encodeheartPopMessage_200,
} from '@/proto/ts/SystemMessage';
import IMessage from '../../IMessage';
export class HeartPopMessage_200 implements IMessage {
  constructor() {
  }

  decode(obj: any) {
    return decodeheartPopMessage_200(obj);
  }

  encode(obj: any) {
    return encodeheartPopMessage_200(obj);
  }
}
