import {
  encodeUserOnlineStatusNotice_1007, decodeUserOnlineStatusNotice_1007,
} from '@/proto/ts/ChannelMessage';
import IMessage from '../../IMessage';
export class UserOnlineStatusNotice_1007 implements IMessage {
  constructor() {
  }

  decode(obj: any) {
    return decodeUserOnlineStatusNotice_1007(obj);
  }

  encode(obj: any) {
    return encodeUserOnlineStatusNotice_1007(obj);
  }
}
