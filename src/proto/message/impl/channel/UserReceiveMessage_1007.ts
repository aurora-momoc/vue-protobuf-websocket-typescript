import {
  encodeUserJoinChannelNotice_1006, decodeUserJoinChannelNotice_1006,
} from '@/proto/ts/ChannelMessage';
import IMessage from '../../IMessage';
export class UserJoinChannelNotice_1006 implements IMessage {
  constructor() {
  }

  decode(obj: any) {
    return decodeUserJoinChannelNotice_1006(obj);
  }

  encode(obj: any) {
    return encodeUserJoinChannelNotice_1006(obj);
  }
}
