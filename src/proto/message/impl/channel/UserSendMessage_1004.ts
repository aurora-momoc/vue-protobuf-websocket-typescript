import {
  encodeUserSendMessage_1004, decodeUserSendMessage_1004,
} from '@/proto/ts/ChannelMessage';
import IMessage from '../../IMessage';
export class UserSendMessage_1004 implements IMessage {
  constructor() {
  }

  decode(obj: any) {
    return decodeUserSendMessage_1004(obj);
  }

  encode(obj: any) {
    return encodeUserSendMessage_1004(obj);
  }
}
