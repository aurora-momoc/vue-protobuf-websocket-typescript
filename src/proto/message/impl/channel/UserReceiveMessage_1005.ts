import {
  encodeUserReceiveMessage_1005, decodeUserReceiveMessage_1005,
} from '@/proto/ts/ChannelMessage';
import IMessage from '../../IMessage';
export class UserReceiveMessage_1005 implements IMessage {
  constructor() {
  }

  decode(obj: any) {
    return decodeUserReceiveMessage_1005(obj);
  }

  encode(obj: any) {
    return encodeUserReceiveMessage_1005(obj);
  }
}
