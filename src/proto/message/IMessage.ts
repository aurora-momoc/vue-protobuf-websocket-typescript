export default interface Message{
    decode(obj: any): any;
    encode(obj: any): any;

}
