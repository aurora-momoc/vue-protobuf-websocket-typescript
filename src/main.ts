import WebsocketUtils from '@/utils/WebsocketUtils';
import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
// import { VirtualList } from 'vue-tiny-virtual-list';

let app = createApp(App)
// 加载.proto文件
app.use(store).use(router).use(() => {
  console.log('test', WebsocketUtils.instance);
})
  .mount('#app');

// app.component('VirtualTinyList', VirtualList)
