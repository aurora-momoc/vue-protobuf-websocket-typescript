interface ApiResult<T> {
  data: T;
  code: number;
  msg: string;
}
export default ApiResult;
