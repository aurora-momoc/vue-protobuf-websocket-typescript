import Long from "long";

class LongUtils{


  constructor() {
  }

  /**
   * proto的Long类型转换为number
   * 但这还是会存在JavaScript处理大数丧失精度的问题，
   * 如果可能会超出JavaScript能够精确表示的范围（超过2的53次方）
   * ，那么这种方法可能会导致错误。这种情况下，可能需要将`channelId`转换为字符串来使用。
   * @param long
   */
  public static protoLongToNumber(long: any): string{
      const longValue = Long.fromBits(long.low, long.high, long.unsigned);
      return longValue.toString();
    // return long.high * Math.pow(2, 32) + long.low;
  }
}


export default LongUtils;
