const RegisterClass = (target: Function) => {
  target.prototype.className = target.name;
}
export default RegisterClass;
