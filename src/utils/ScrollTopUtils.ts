class ScrollTopUtils {



  //获取滚动条被卷进去的高
  public  static getScrollTop = (dom) => {
    let scrollTop = 0;
    if (typeof dom.scrollTop !== "undefined") {
      // 如果浏览器支持直接获取scrollTop属性
      scrollTop = dom.scrollTop;
    } else if (typeof dom.pageYOffset !== "undefined") {
      // 如果浏览器支持pageYOffset属性
      scrollTop = dom.pageYOffset;
    } else if (typeof dom.scrollY !== "undefined") {
      // 如果浏览器支持scrollY属性
      scrollTop = dom.scrollY;
    } else if (typeof document.documentElement.scrollTop !== "undefined") {
      // 兼容IE浏览器的获取scrollTop属性
      scrollTop = document.documentElement.scrollTop;
    } else if (typeof document.body.scrollTop !== "undefined") {
      // 兼容IE浏览器的获取scrollTop属性
      scrollTop = document.body.scrollTop;
    }
    return scrollTop;
  }
  //获取可滚动高度
  public  static getScrollHeight = (dom) => {
    var scrollHeight = 0;
    if (typeof dom.scrollHeight !== "undefined") {
      // 如果浏览器支持直接获取scrollHeight属性
      scrollHeight = dom.scrollHeight;
    } else if (typeof dom.offsetHeight !== "undefined") {
      // 如果浏览器支持offsetHeight属性
      scrollHeight = dom.offsetHeight;
    } else if (typeof dom.clientHeight !== "undefined") {
      // 如果浏览器支持clientHeight属性
      scrollHeight = dom.clientHeight;
    }
    return scrollHeight;

  }
  //获取可见区域高
  public  static getClientHeight = (dom) => {
    var clientHeight = 0;
    if (typeof dom.clientHeight !== "undefined") {
      // 如果浏览器支持直接获取clientHeight属性
      clientHeight = dom.clientHeight;
    } else if (typeof dom.offsetHeight !== "undefined") {
      // 如果浏览器支持offsetHeight属性
      clientHeight = dom.offsetHeight;
    }
    return clientHeight;
  }
}
export default ScrollTopUtils;
