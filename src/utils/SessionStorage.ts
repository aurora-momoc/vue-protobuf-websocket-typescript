export default class {
  public static save(key: string, value: any) {
    window.sessionStorage.setItem(key, JSON.stringify(value));
  }

  public static get(key: string) {
    const json: string = window.sessionStorage.getItem(key) || '';
    if (!json) {
      return null;
    }
    return JSON.parse(json);
  }

  public static remove(key: string) {
    window.sessionStorage.removeItem(key);
  }

  public static clear() {
    window.sessionStorage.clear();
  }
}
