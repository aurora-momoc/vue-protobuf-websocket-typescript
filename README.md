# aurora-chat-web-typescript

在线示例：https://server.668628.xyz

### 一键安装依赖启动
```shell
npm install -g vite yarn
# windows 需要进入
# cmd执行一下命令
cmd
yarn
yarn dev
```

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
## 关于vue的一些路由教程

https://blog.csdn.net/wulala_hei/article/details/80488727


## 无限滚动组件
https://github.com/keno-lee/vue-tiny-virtual-list/blob/master/README_cn.md

### 生成proto.js
```shell script
npx pbjs -t json-module -w commonjs -o src/proto/proto.js src/proto/*.proto
```
### 解决long类型问题
```js
const protobuf = require("protobufjs");
protobuf.util.Long = require("long"); // 需要依赖 "long"模块
protobuf.configure();
```
```shell script
npx pbjs --ts src/proto/model/ChannelMessage.ts src/proto/ChannelMessage.proto
npx pbjs --ts src/proto/model/SystemMessage.ts src/proto/SystemMessage.proto
npx pbjs --ts src/proto/model/UserMessage.ts src/proto/UserMessage.proto
```
### 生成Ts代码
```shell script
npx pbjs --ts src/proto/model/proto.d.ts src/proto/*.proto
```

### 环境变量的使用
```
https://juejin.cn/post/7094940781726826526#heading-3
```

###  表情包开源组件
```
https://github.com/serebrov/emoji-mart-vue
```