/* eslint-disable */
import * as path from 'path';
import vue from '@vitejs/plugin-vue'
import Components from 'unplugin-vue-components/vite';
import { VantResolver } from '@vant/auto-import-resolver';
import { resolve } from 'path'



// @see https://cn.vitejs.dev/config/
export default ({
  command,
  mode,
}) => {
  let rollupOptions = {};


  let optimizeDeps = {};


  let alias = {
    '.git': path.resolve(__dirname, './.git'),
    'node_modules': path.resolve(__dirname, './node_modules'),
    'public': path.resolve(__dirname, './public'),
    'src': path.resolve(__dirname, './src'),
    'tests': path.resolve(__dirname, './tests'),
    '@': path.resolve(__dirname, './src'),
    'vue$': 'vue/dist/vue.runtime.esm-bundler.js',
  }

  let proxy = {
    '/api': {
      "target": "http://127.0.0.1:8080",
      "changeOrigin": true,
      "secure": false,
      "pathRewrite": {
        "^/api": "/api"
      }
    },
  }

  // todo 替换为原有变量
  let define = {
    // '__VUE_OPTIONS_API__.0': 't',
    // '__VUE_OPTIONS_API__.1': 'r',
    // '__VUE_OPTIONS_API__.2': 'u',
    // '__VUE_OPTIONS_API__.3': 'e',
    // '__VUE_PROD_DEVTOOLS__.0': 'f',
    // '__VUE_PROD_DEVTOOLS__.1': 'a',
    // '__VUE_PROD_DEVTOOLS__.2': 'l',
    // '__VUE_PROD_DEVTOOLS__.3': 's',
    // '__VUE_PROD_DEVTOOLS__.4': 'e',
    // 'process.env.NODE_ENV': '"development"',
  }

  let esbuild = {}

  return {
    base: './', // index.html文件所在位置
    root: './', // js导入的资源路径，src
    resolve: {
      alias,
    },
    define: define,
    server: {
      port: 9000,
      // 代理
      proxy: proxy,
    },
    build: {
      target: 'es2015',
      minify: 'terser', // 是否进行压缩,boolean | 'terser' | 'esbuild',默认使用terser
      manifest: false, // 是否产出maifest.json
      sourcemap: false, // 是否产出soucemap.json
      outDir: 'build', // 产出目录
      rollupOptions,
    },
    esbuild,
    optimizeDeps,
    plugins: [
      vue(),
      Components({
        resolvers: [VantResolver()],
      }),
    ],
    css: {
      preprocessorOptions: {
        less: {
          // 支持内联 JavaScript
          javascriptEnabled: true,
        },
      },
    },
  };
};
